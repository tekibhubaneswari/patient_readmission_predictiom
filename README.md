# Patient Readmission Prediction

## Table of Contents

- [Project Overview](#project-overview)
- [Motivation](#motivation)
- [Dataset](#dataset)
- [Project Directory](#project-directory)
- [Data Preprocessing](#data-preprocessing)
- [Modeling](#modeling)
- [Evaluation](#evaluation)
- [Results](#results)
- [Conclusion](#conclusion)
- [Future Scope](#future-scope)
- [How to Run the Project](#how-to-run-the-project)
- [Acknowledgments](#acknowledgments)

## Project Overview

The Patient Readmission Prediction project aims to predict whether a patient will be readmitted to the hospital within 30 days of discharge. By leveraging machine learning techniques, this project seeks to identify patients at risk of readmission, enabling healthcare providers to intervene proactively.

## Motivation

Reducing hospital readmissions is a critical goal for healthcare systems worldwide. High readmission rates not only strain healthcare resources but also negatively impact patient outcomes. Accurate prediction models can help healthcare providers identify high-risk patients and implement targeted interventions to reduce readmissions, thereby improving patient care and reducing costs.

## Dataset

The dataset used for this project contains records of patient admissions, including various demographic, clinical, and hospital-related variables. Key features include:

- Patient demographics (age, gender, etc.)
- Medical history (diagnoses, previous admissions, etc.)
- Hospital-related information (length of stay, discharge disposition, etc.)
- Readmission indicator (target variable)

## Project Directory
```
patient_readmission_prediction/
├── assets/
│ └── *.png # Data visualization files
├── data/
│ ├── raw/
│ │ └── hospital_readmissions.csv # Original dataset
│ ├── processed/
│ │ ├── X_train.csv
│ │ ├── X_test.csv
│ │ ├── y_train.csv
│ │ └── y_test.csv
├── models/
│ └── RandomForest_model.joblib # Trained model file
├── notebooks/
│ ├── preprocessing.ipynb
| ├── training.ipynb
| └── data_visualization.ipynb
├── src/
│ 
│ ├── data_visualisation.py
│ └── training.py
├── requirements.txt # Project dependencies
└── README.md
```

## Data Preprocessing

Data preprocessing is a crucial step in preparing the dataset for modeling. The following steps were performed:
1. **Data Cleaning**: Handling missing values, outliers, and inconsistent data entries.
2. **Data Transformation**: Converting categorical variables to numerical representations using techniques like one-hot encoding.
3. **Normalization**: Scaling numerical features to ensure they contribute equally to the model's performance.

## Modeling

Several machine learning models were explored for predicting patient readmissions, including:

- Logistic Regression
- Decision Trees
- Random Forest
- Support Vector Machines (SVM)
- K-Nearest Neighbors (KNN)

The models were trained using a pipeline that included data preprocessing steps. The pipeline ensures consistent preprocessing during training and prediction.

## Evaluation

Model evaluation was conducted using the following metrics:

- **Accuracy**: The proportion of correctly predicted instances among the total instances.
- **Precision, Recall, F1-Score**: Detailed performance metrics for each class.
- **Confusion Matrix**: A matrix showing the true positive, true negative, false positive, and false negative predictions.

## Results

The results of the model evaluations were as follows:

- The Random Forest model achieved the highest accuracy.
- Detailed performance metrics are provided in the classification report.
- The confusion matrix highlights the distribution of correct and incorrect predictions.

## Conclusion

The project successfully developed a machine learning model capable of predicting patient readmissions with high accuracy. By identifying high-risk patients, healthcare providers can implement targeted interventions to reduce readmissions, improving patient outcomes and reducing costs.

## Future Scope

Future work on this project can focus on:

- **Hyperparameter Tuning**: Optimizing model parameters to further improve performance.
- **Feature Engineering**: Creating new features from existing data to better capture underlying patterns.
- **Model Interpretability**: Developing methods to interpret model predictions and provide actionable insights to healthcare providers.
- **Integration with Hospital Systems**: Implementing the model in a real-world healthcare setting to provide real-time readmission risk predictions.

## How to Run the Project

To run the project, follow these steps:

1. **Clone the Repository**:
    ```sh
    git clone https://github.com/tekibhubaneswari/patient_readmission_prediction.git
    cd patient_readmission_prediction
    ```

2. **Install Dependencies**:
    ```sh
    pip install -r requirements.txt
    ```

3. **Prepare the Data**:
    Place the dataset in the `data/processed/` directory.

4. **Run the Training Script**:
    ```sh
    python src/training.py
    ```

5. **Evaluate the Model**:
    The results will be printed in the console, including accuracy, classification report, and confusion matrix.

## Acknowledgments

Special thanks to the healthcare professionals and data scientists who provided valuable insights and feedback throughout the development of this project.
