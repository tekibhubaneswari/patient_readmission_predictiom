# DATA VISUALISATION

## Table of Contents

- [Histogram](#histogram)
- [Heat Map](#heat-map)
- [Boxplot](#box-plot)
- [Countplot](#count-plot)

## Histogram
!["alternative text"](Histplot_of_time_in_hospital_and_n_lab_procedures.png "histplot of time_in_hospital and n_lab_procedures")

### Left Histogram: Time in Hospital
    Title: "Histplot of time_in_hospital"
    X-axis: "time_in_hospital" (number of days patients spend in the hospital)
    Y-axis: "Count" (number of patients)
    Description: The histogram shows that most patients spend between 1 and 5 days in the hospital. The number of patients decreases as the length of stay increases. Very few patients stay longer than 10 days.
### Right Histogram: Number of Lab Procedures
    Title: "Histplot of n_lab_procedures"
    X-axis: "n_lab_procedures" (number of lab procedures a patient undergoes)
    Y-axis: "Count" (number of patients)
    Description: The histogram shows that the number of lab procedures most commonly falls between 20 and 60. The distribution peaks around 40 procedures, indicating that this is the most common number of procedures. The number of patients with lab procedures above 60 decreases gradually.

!["alternative text"](Histplot_of_n_procedures_and_n_medications.png "histplot of n_procedures and n_medications")

### Left Histogram: Number of Procedures
    Title: "Histplot of n_procedures"
    X-axis: "n_procedures" (number of medical procedures a patient undergoes)
    Y-axis: "Count" (number of patients)
    Description: The histogram shows that most patients do not undergo any procedures (value 0). The number of patients decreases with the increase in the number of procedures. A smaller peak is observed at one procedure, and the numbers continue to drop as the procedure count increases.
### Right Histogram: Number of Medications
    Title: "Histplot of n_medications"
    X-axis: "n_medications" (number of medications a patient receives)
    Y-axis: "Count" (number of patients)
    Description: The histogram shows that most patients receive between 0 and 20 medications, with a peak around 10 medications. The number of patients decreases as the number of medications increases. Few patients receive more than 30 medications.

!["alternative text"](Histplot_of_n_outpatient_and_n_inpatient.png "histplot of n_outpatients and n_inpatient")

### Left Histogram: Number of Outpatient Visits
    Title: "Histplot of n_outpatient"
    X-axis: "n_outpatient" (number of outpatient visits a patient has)
    Y-axis: "Count" (number of patients)
    Description: The histogram shows that the majority of patients have between 0 and 1 outpatient visits. The count drops significantly as the number of outpatient visits increases. Very few patients have more than 5 outpatient visits.
### Right Histogram: Number of Inpatient Visits
    Title: "Histplot of n_inpatient"
    X-axis: "n_inpatient" (number of inpatient visits a patient has)
    Y-axis: "Count" (number of patients)
    Description: The histogram shows that the majority of patients have 0 inpatient visits. There is a significant drop in the number of patients as the number of inpatient visits increases. Very few patients have more than 2 inpatient visits.


!["alternative text"](Histplot_of_n_emergency_and_time_in_hospital.png "histplot of n_outpatients and n_inpatient")

### Left Histogram: n_emergency
    Title: "Histplot of n_emergency"
    X-axis: "n_emergency" (number of emergencies)
    Y-axis: "Count" (number of patients)
    Description: The histogram shows that the majority of patients have between 0 and 1 outpatient visits. The count drops significantly as the number of emergency visits increases. 
### Right Histogram : time_in_hospital
    Title: "Histplot of time_in_hospital"
    X-axis: "time_in_hospital" (number of emergencies)
    Y-axis: "Count" (number of patients)
    Description: The histogram shows that the majority of patients have between 0 and 1 outpatient visits. The count drops significantly as the number of time_in_hospital visits increases.

## Heat Map
!["alternative text"](heat_map_of_numeric_features.png "heat map of numeric features")  

In this  heatmap, the features are things like time spent in the hospital, number of lab procedures performed, and number of medications prescribed. The colors represent the correlation coefficient, which is a number between -1 and 1. A higher positive number (shown in red) indicates a stronger positive correlation, meaning the two variables tend to increase together. A lower negative number (shown in blue) indicates a stronger negative correlation, meaning one variable tends to decrease as the other increases. A value close to zero (shown in white) indicates no correlation.

## Boxplot

!["altenative text"](Box_plot_of_n_emergency_and_time_in_hospital.png "box plot of no of emergencies and time in hosplital")

### Left box plot
    Title: Box Plot of n_emergency
    X-axis: n_emergency (label is missing units)
    Y-axis: Count
    Description: The box plot of n_emergency shows the distribution of a variable called "n_emergency". The x-axis shows the number of people who were in some emergency situation, but the label does not specify what units are used to measure "n_emergency". The scale goes from 10 to 60. Without knowing the units, it is difficult to interpret the meaning of the values on the axis. For example, it could be the number of people who came into the emergency room in a single day, or it could be the number of emergencies a healthcare provider has responded to in a week.

### Right box plot
    Title: Box Plot of time in hospital
    X-axis: Time in hospital (hours)
    Y-axis: Count
    Description: The box plot of time in hospital shows the distribution of the amount of time people spent in the hospital. The x-axis is labeled "time in hospital" and the scale goes from 2 to 14, with the units being hours. The vertical axis shows the count, or the number of people who fall into each category on the x-axis.

!["altenative text"](Box_plot_of_n_outpatient_and_n_inpatient.png "box plot of no of outpatient and inpatient")

### Left box plot
    Title: Box Plot of n_outpatient
    X-axis: Number of outpatients
    Y-axis: Count
    Description: The box plot of n_outpatient shows the distribution of the number of outpatients seen by a healthcare provider. The x-axis shows the number of outpatients. The vertical axis shows the count, or the number of healthcare providers who saw that many outpatients.

### Right box plot
    Title: Box Plot of n_inpatient
    X-axis: Number of inpatients
    Y-axis: Count
    Description: The box plot of n_inpatient shows the distribution of the number of inpatients seen by a healthcare provider. The x-axis shows the number of inpatients. The vertical axis shows the count, or the number of healthcare providers who saw that many inpatients.

!["altenative text"](Box_plot_of_n_procedures_and_n_medications.png "box plot of no of procedures and medications")

### Left box plot
    Title: Box Plot of n_procedures
    X-axis: Number of procedures (unlabeled units)
    Y-axis: Count
    Description: The box plot of n_procedures shows the distribution of a variable called "n_procedures". The x-axis shows the number of procedures performed on a patient. The label for the x-axis does not specify what units are used to measure "n_procedures". The scale goes from 10 to 70. Without knowing the units, it is difficult to interpret the meaning of the values on the axis. For example, it could be the number of medical procedures a patient underwent during a hospital stay, or it could be the number of procedures a doctor performed in a single day.

### Right box plot
    Title: Box Plot of n_medications
    X-axis: Number of medications
    Y-axis: Count
    Description: The box plot of n_medications shows the distribution of the number of medications prescribed to a patient. The x-axis shows the number of medications. The vertical axis shows the count, or the number of patients who were prescribed that many medications.

## Countplot

!["altenative text"](Countplot_of_A1Ctest_and_change.png "count plot of no of A1C Test and change")

### Left count plot
    Title: Countplot of Alctest
    X-axis: Alc test results (no, normal, high)
    Y-axis: Count
    Description: This count plot shows the number of patients who received different results on an Alc test. Alc test is likely a blood test used to diagnose or monitor a condition. The x-axis categorizes the results of the test into three groups: no, normal, and high. The y-axis shows the count, or the number of patients who fall into each category. For example, the plot shows that there were more patients with a normal Alc test result than any other category.

### Right count plot
    Title: Countplot of Change
    X-axis: Change (yes, no)
    Y-axis: Count
    Description: This count plot shows the number of patients who did or did not experience a change in condition. The x-axis is labeled "Change" with two categories: yes and no. The y-axis shows the count, or the number of patients who fall into each category. For example, the plot shows that there were more patients who did not experience a change in condition than those who did.

!["alternative text"](Countplot_of_age_and_medical_specialty.png "count plot of the age and medocal speciality")

### Left count plot
    Title: Countplot of age
    X-axis: Age groups (70-80, 60-70, 50-60, 40-50, 80-90, 90-100)
    Y-axis: Count
    Description: This count plot shows the number of people in each age group. The x-axis categorizes people into different age groups. The y-axis shows the count, or the number of people who fall into each age group. For example, the plot shows that there are more people in the 40-50 age group than any other group.

### Right count plot
    Title: Countplot of medical specialty
    X-axis: Medical specialty (Missing, Other, Internal Medicine, Family/General Practice, Cardiology, Surgery, Emergency/Trauma)
    Y-axis: Count
    Description: This count plot shows the number of people categorized by their medical specialty. The x-axis lists the different medical specialties. The y-axis shows the count, or the number of people who listed each specialty. For example, the plot shows that there are more people in the Internal Medicine specialty than any other specialty. It also shows that there is a category labeled "Missing" which means that some data points were missing information about the medical specialty.


!["alternative text"](Countplot_of_diabetes_med_and_readmitted.png "count plot of the age and medocal speciality")

### Left count plot
    Title: Countplot of diabetes_med
    X-axis: Diabetes medication (yes, no)
    Y-axis: Count
    Description: This count plot shows the number of people who take diabetes medication compared to those who do not. The x-axis categorizes people based on whether they take diabetes medication or not. The y-axis shows the count, or the number of people who fall into each category. For example, the plot shows that there are more people who do not take diabetes medication than those who do.

### Right count plot
    Title: Countplot of readmitted
    -axis: Readmitted (yes, no)
    Y-axis: Count
    Description: This count plot shows the number of people who were readmitted to the hospital compared to those who were not. The x-axis categorizes people based on whether they were readmitted to the hospital or not. The y-axis shows the count, or the number of people who fall into each category. For example, the plot shows that there are more people who were not readmitted to the hospital than those who were.
