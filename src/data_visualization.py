import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

assets_dir = "C:/Users/tekib/OneDrive/Desktop/workspace/patient_readmission_predictiom/assets"
if not os.path.exists(assets_dir):
    os.makedirs(assets_dir)


num_features = ['time_in_hospital', 'n_lab_procedures', 'n_procedures', 'n_medications',
                'n_outpatient', 'n_inpatient', 'n_emergency']
cat_features = ['age', 'medical_specialty', 'diag_1', 'diag_2', 'diag_3', 'glucose_test',
                'A1Ctest', 'change', 'diabetes_med', 'readmitted']
df = pd.read_csv('C:/Users/tekib/OneDrive/Desktop/workspace/patient_readmission_predictiom/data/raw/hospital_readmissions.csv')

def save_plot(fig, filename):
    fig.savefig(f"{assets_dir}/{filename}")

def box_plot(data, column1, column2):
    fig, axs = plt.subplots(1, 2, figsize=(16, 6))
    sns.boxplot(data=data, x=column1, ax=axs[0])
    sns.boxplot(data=data, x=column2, ax=axs[1])
    axs[0].set_title(f"Box Plot of {column1}")
    axs[1].set_title(f"Box Plot of {column2}")
    save_plot(fig, f"Box_plot_of_{column1}_and_{column2}.png")
    plt.show()

def heatmap(data, num_features):
    plt.figure(figsize=(10, 8))
    sns.heatmap(df[num_features].corr(), annot=True, linewidth=.5, fmt=".1f")
    plt.title("Heatmap of the Numeric Features")
    save_plot(plt, f"heat_map_of_numeric_features.png")
    plt.show()

def hist_plot(data, column1, column2):
    fig, axs = plt.subplots(1, 2, figsize=(16, 6))
    sns.histplot(data=data, x=column1, kde=True, ax=axs[0])
    sns.histplot(data=data, x=column2, kde=True, ax=axs[1])
    axs[0].set_title(f"Histplot of {column1}")
    axs[1].set_title(f"Histplot of {column2}")
    save_plot(fig, f"Histplot_of_{column1}_and_{column2}.png")
    plt.show()

def count_plot(data, column1, column2):
    fig, axs = plt.subplots(1, 2, figsize=(16, 6))
    sns.countplot(data=data, y=column1, ax=axs[0])
    sns.countplot(data=data, y=column2, ax=axs[1])
    axs[0].set_title(f"Countplot of {column1}")
    axs[1].set_title(f"Countplot of {column2}")
    save_plot(fig, f"Countplot_of_{column1}_and_{column2}.png")
    plt.show()

if __name__ == "__main__":
    box_plot(df, 'time_in_hospital', 'n_lab_procedures')
    box_plot(df, 'n_procedures', 'n_medications')
    box_plot(df, 'n_outpatient', 'n_inpatient')
    box_plot(df, 'n_emergency', 'time_in_hospital')

    hist_plot(df, 'time_in_hospital', 'n_lab_procedures')
    hist_plot(df, 'n_procedures', 'n_medications')
    hist_plot(df, 'n_outpatient', 'n_inpatient')
    hist_plot(df, 'n_emergency', 'time_in_hospital')

    for i in range(0, len(cat_features), 2):
        if i + 1 < len(cat_features):
            count_plot(df, cat_features[i], cat_features[i + 1])
        else:
            count_plot(df, cat_features[i], cat_features[i])  # Just to handle odd number of features

    heatmap(df, num_features)
